#Chubonyo Config
import aiohttp
import json
import urllib3


async def fetch(url: str, method: str = "GET", params=None):
    url = urllib3.util.parse_url(url)
    async with aiohttp.ClientSession() as session:
        if method == "POST":
            async with session.post(url=str(url), data=params) as response:
                return await response.json()
        else:
            async with session.get(str(url)) as response:
                print(response)
                print(await response.json())