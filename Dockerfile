FROM        python:3.8.3
COPY        . /chubot
WORKDIR     /chubot
RUN         pip install -Ur requirements.txt
ENTRYPOINT  git fetch -ap && git clean -f && git reset --hard origin/master && python -m chubot.py
