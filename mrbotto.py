import discord
import asyncpg
import libneko
import typing

from discord.ext import commands


class MrB(libneko.clients.Bot):
    def __init__(self):
        super().__init__(command_prefix=self.get_prefix, case_insensitive=True)
        self.db = False

    async def start(self, *args, **kwargs):
        try:
            self.db = await asyncpg.create_pool(
            user=os.environ["POSTGRES_USER"],
            password=os.environ["POSTGRES_PASSWORD"],
            database=os.environ["POSTGRES_DB"],
            host=os.getenv("POSTGRES_HOST", "localhost"),
            port=5432,
        )
        except:
            self.db = False

        await super().start(*args, **kwargs)

    async def close(self, *args, **kwargs):
        await self.session.close()

        await super().close(*args,**kwargs)
    def get_cog(self, name: str) -> commands.Cog:  # will override the existing behaviour
        """Gets a cog by its name, ignoring casing."""
        for n in self.cogs:
            if n.casefold() == name.casefold():
                return self.cogs[n]

    async def get_prefix(bot, message):
        default_prefix = "c!"
        if bot.db:
            await bot.wait_until_ready()
            async with bot.db.acquire() as conn:
                try:
                    result = await conn.fetchval(
                        f"SELECT prefix FROM prefixes WHERE guild_id = $1", message.guild.id
                    )
                except AttributeError as e:
                    return commands.when_mentioned_or(default_prefix)(bot, message)
            if result is None:
                return commands.when_mentioned_or(default_prefix)(bot, message)
            else:
                return commands.when_mentioned_or(result)(bot, message)
        else:
            return commands.when_mentioned_or(default_prefix)(bot, message)
