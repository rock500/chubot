#Chubonyo App
import logging
import os
import json

from discord.ext import commands
from mrbotto import MrB
import asyncpg
import discord


# Default prefix = 'c!'
bot = MrB()
logging.basicConfig(level=os.getenv("LOGGER_VERBOSITY", "INFO").upper())
logger = logging.getLogger("chubonyo_bot")
bot.remove_command('help')

# To get stuff like the token.
with open("Config.json") as fp:
    bot.config = json.load(fp)


# all the command extensions :)
extensions = [
    "exts.admin",
    "exts.emotes",
    "exts.exec",
    "exts.fun",
    "exts.handlers",
    "exts.misc",
    "exts.moderation",
    "exts.nsfw",
    "exts.spechelp",
    "exts.tests",
    "exts.utility",
]


@bot.listen()
async def on_start():
    bot.owner = (await bot.application_info()).owner
    await bot.db.execute("""
            CREATE TABLE IF NOT EXISTS prefixes (
                 guild_id    BIGINT    PRIMARY KEY,
                 prefix      VARCHAR(5),

                 CONSTRAINT valid_prefix_chk  CHECK  (TRIM(prefix) <> '')
            );
        """)


@bot.event  # when the bot starts up it prints some handy info and changes the activity presence
async def on_ready():
    print(f'Logged in as: {bot.user.name}')
    print(f'With ID: {bot.user.id}\n')
    await bot.change_presence(activity=discord.Game('Use c!help | Connected to ' + str(sum(1 for _ in bot.get_all_members())) + ' users in ' + str(len(bot.guilds)) + ' guilds'))


@bot.listen() #auto reactions to messages
async def on_message(message):
    if not message.author.bot:
        if message.content == "Chubonyo" or message.content == "Chuboñyo":
            await message.channel.send("<3 (´∩｡• ᵕ •｡∩`) ♡")
    else:
        return
    if 332857142522413056 in message.raw_mentions:
        if not message.author.bot:
            await message.add_reaction('\N{HEAVY BLACK HEART}\N{VARIATION SELECTOR-16}')
        else:
            return
    if 702011241958473776 in message.raw_mentions:
        if not message.author.bot:
            await message.add_reaction('\N{BABY ANGEL}')
        else:
            return


# Loads all extensions
for ext in extensions:
    try:
        bot.load_extension(ext)
    except commands.errors.ExtensionNotFound:
        logging.warning("Extension `%s` was not found.", ext)
    except commands.errors.ExtensionFailed as error:
        logging.error("Failed to load extension `%s`, reason: %s", ext, error.__cause__)
    except commands.errors.NoEntryPointError:
        logging.warning("Extension `%s` has no setup function!", ext)


bot.run(bot.config["token"])

