import aiohttp
from discord.ext import commands
import os
import datetime
import discord
import random


class Emotes(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # confused emote command
    async def confused(self, ctx):
        """I'm so confused"""
        img2 = random.choice([
            "https://cdn.discordapp.com/attachments/339112602867204097/722804054467543160/confused0.gif",
            "https://cdn.discordapp.com/attachments/339112602867204097/722803820517785611/confused1.gif",
            "https://cdn.discordapp.com/attachments/339112602867204097/722803929246859404/confused2.gif",
            "https://cdn.discordapp.com/attachments/339112602867204097/722803727970598922/confused3.png"
        ])
        embed = discord.Embed(
            title="Uhh...",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_image(url=img2)
        await ctx.send("`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # cry emote command
    async def cry(self, ctx):
        """Cry... are you ok? :c"""
        url2 = random.choice(
            ["https://media1.tenor.com/images/b88fa314f0f172832a5f41fce111f359/tenor.gif?itemid=13356071",
             "https://media1.tenor.com/images/09b085a6b0b33a9a9c8529a3d2ee1914/tenor.gif?itemid=5648908",
             "https://media.tenor.com/images/bf139869d81cd9b73144d6b941ebb733/tenor.gif",
             "https://media1.tenor.com/images/10780bdf3aa8476984624bf21b764afc/tenor.gif?itemid=14165150",
             "https://pa1.narvii.com/5624/2a327712716ceda09677e0d7927bcdd479e26b0a_00.gif",
             "https://media.tenor.com/images/19089cd2b4970740debff2cdfc43329a/tenor.gif",
             "https://media.giphy.com/media/4pk6ba2LUEMi4/giphy.gif",
             "https://24.media.tumblr.com/b7ae6c694085e0b294cdd938278c70c7/tumblr_mpupx1krXM1s3jc4vo1_400.gif",
             "https://media1.tenor.com/images/3831c47d6cf85b7055d63fef9ce44add/tenor.gif?itemid=6004314",
             "https://media1.tenor.com/images/354f46fa4b60e77eb2dc59cc956e49b5/tenor.gif?itemid=10518252",
             "https://i.pinimg.com/originals/be/6c/6a/be6c6a8be82fa3bea43650281a893e87.gif",
             "https://media1.tenor.com/images/87ef2f7663b9dc4bf39b7e9481cda842/tenor.gif?itemid=5086387",
             "https://media1.tenor.com/images/75edc9882e5175f86c2af777ffbb14a6/tenor.gif?itemid=5755232",
             "https://media1.tenor.com/images/28f4e412fb32a2191841c90bb27d54df/tenor.gif?itemid=12309024",
             "https://cdn.weeb.sh/images/Sy1EUa-Zz.gif",
             "https://cdn.weeb.sh/images/ryap_aEC-.gif",
             "https://cdn.weeb.sh/images/rJUujgJ5Z.gif",
             "https://cdn.weeb.sh/images/H1tfQI7wZ.gif",
             "https://i.pinimg.com/originals/5a/87/dc/5a87dca264c8a2c651eed4a4cb5228e9.gif"])
        embed = discord.Embed(
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_author(name=f"{ctx.author.display_name} is crying!", url=ctx.author.avatar_url,
                         icon_url=ctx.author.avatar_url)
        embed.set_image(url=f"{url2}")
        await ctx.send("`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # dance emote command
    async def dance(self, ctx):
        """Move it and grove it!"""
        url3 = random.choice(
            ["https://cdn.discordapp.com/avatars/471538966517121035/a_ebb3134edd6a73db504901a9bc58dcb2.gif?size=2048",
             "https://media.tenor.com/images/fe3826b59f80f5e6c7cc04eb474fb44d/tenor.gif",
             "https://media.tenor.com/images/7fa3b39ddac5925af0d81aefeeeb3ad4/tenor.gif",
             "https://media1.tenor.com/images/c925511d32350cc04411756d623ebad6/tenor.gif?itemid=13462237",
             "https://i.imgur.com/BbIar.gif",
             "https://24.media.tumblr.com/94f66d1090d575a2dda0f12bc1b6c725/tumblr_mkhmxtOwNI1rg5pb9o1_500.gif",
             "https://media.giphy.com/media/jCaU8WfesJfH2/giphy.gif",
             "https://i.gifer.com/Afdv.gif",
             "https://i.ya-webdesign.com/images/anime-dancing-gif-png-2.gif",
             "https://media.giphy.com/media/euMGM3uD3NHva/giphy.gif",
             "https://i.kym-cdn.com/photos/images/newsfeed/001/115/816/936.gif",
             "https://media.tenor.com/images/279bf22997edc0703cee75385645d7f8/tenor.gif",
             "https://media.giphy.com/media/LKh0Qdj7pR7aM/giphy.gif",
             "https://i.pinimg.com/originals/81/df/5e/81df5e907f81dad1721f398ed7408deb.gif",
             "https://cdn.weeb.sh/images/ByOruIQPb.gif",
             "https://cdn.weeb.sh/images/SkG7OIQDZ.gif",
             "https://cdn.weeb.sh/images/H1tktLQvZ.gif",
             "https://cdn.weeb.sh/images/rJDd_IXvb.gif",
             "https://cdn.weeb.sh/images/BkmPO8Xwb.gif",
             "https://cdn.weeb.sh/images/rJdiOUXwW.gif",
             "https://qsf5.com/wp-content/uploads/gallery/random-gallery/mashiro-dance.gif"])
        embed = discord.Embed(
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_author(name=f"{ctx.author.display_name} is dancing!", url=ctx.author.avatar_url,
                         icon_url=ctx.author.avatar_url)
        embed.set_image(url=f"{url3}")
        await ctx.send("`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['interesting', 'hm', 'icarly', 'i-carly'])  # cry emote command
    async def hmm(self, ctx):
        """Very interesting."""
        embed = discord.Embed(
            title="Hmm.. Interesting",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_image(url="https://cdn.discordapp.com/attachments/491637007466889257/712701342849957989/5e87db3b9d84ea88ce4caf4840c6929e.png")
        await ctx.send("`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # dance emote command
    async def hug(self, ctx, user: discord.Member = None):
        """Hug someone! c:"""
        async with aiohttp.request("get", f"https://nekos.life/api/v2/img/hug") as resp:
            nekos = await resp.json()
            if user is None:
                user = self.bot.user
        embed = discord.Embed(
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_author(name=f"{ctx.author.display_name} hugs {user.display_name}",
                         url=ctx.author.avatar_url,
                         icon_url=ctx.author.avatar_url
                         )
        embed.set_image(url=nekos["url"])
        await ctx.send(content="`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['pet'])  # dance emote command
    async def pat(self, ctx, user: discord.Member = None):
        """Pet someone! c:"""
        async with aiohttp.request("get", f"https://nekos.life/api/v2/img/pat") as resp:
            nekos = await resp.json()
            if user is None:
                user = self.bot.user
        embed = discord.Embed(
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_author(name=f"{ctx.author.display_name} pets {user.display_name}! so cute c:",
                         url=ctx.author.avatar_url,
                         icon_url=ctx.author.avatar_url
                         )
        embed.set_image(url=nekos["url"])
        await ctx.send(content="`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # dance emote command
    async def smug(self, ctx):
        """So smug! :c"""
        async with aiohttp.request("get", f"https://nekos.life/api/v2/img/smug") as resp:
            nekos = await resp.json()
        embed = discord.Embed(
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_author(name=f"{ctx.author.display_name} is so smug!")
        embed.set_image(url=nekos["url"])
        await ctx.send(content="`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # sike emote command
    async def sike(self, ctx):
        """Sike, you thought!"""
        img1 = random.choice([
            "https://cdn.discordapp.com/attachments/339112602867204097/722802659446226994/Sike0.jpg"
        ])
        embed = discord.Embed(
            title="Sike!",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_image(url=img1)
        await ctx.send("`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # what emote command
    async def what(self, ctx):
        """Display just how confused you are."""
        img = random.choice([
            "https://cdn.discordapp.com/attachments/339112602867204097/722801077119877140/what0.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801230346190908/what1.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801329860247632/what2.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801329294016522/what3.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801332247068703/what4.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801318636290118/what5.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801806148632646/what6.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801808199778354/what7.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801811836370984/what8.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801817469059132/what9.jpg",
            "https://cdn.discordapp.com/attachments/339112602867204097/722801820203745360/what10.jpg"
        ])
        embed = discord.Embed(
            title="Wait... What?",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        embed.set_image(url=img)
        await ctx.send("`Tip:` want to suggest an emote? use the `c!suggest` command", embed=embed)


def setup(bot):
    bot.add_cog(Emotes(bot))
