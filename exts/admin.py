import inspect
import random
import sys
import traceback
import discord
import datetime
import os
import json

import libneko
from discord.ext import commands
import asyncio


#def countlines(start, lines=0, header=True, begin_start=None): #counts the total amount of lines in this file and sub-files
#    if header:
#        print('{:>10} |{:>10} | {:<20}'.format('ADDED', 'TOTAL', 'FILE'))
#        print('{:->11}|{:->11}|{:->20}'.format('', '', ''))
#
#    for count1 in os.listdir(start):
#        count1 = os.path.join(start, count1)
#        if os.path.isfile(count1):
#            if count1.endswith('.py'):
#                with open(count1, 'r') as f:
#                    newlines = f.readlines()
#                    newlines = len(newlines)
#                    lines += newlines
#
#                    if begin_start is not None:
#                        reldir_of_thing = '.' + count1.replace(begin_start, '')
#                    else:
#                        reldir_of_thing = '.' + count1.replace(start, '')
#
#                    print('{:>10} |{:>10} | {:<20}'.format(
#                            newlines, lines, reldir_of_thing))
#
#    for count1 in os.listdir(start):
#        count1 = os.path.join(start, count1)
#        if os.path.isdir(count1):
#            lines = countlines(count1, lines, header=False, begin_start=start)
#
#    return lines
#
#countlines(r'C:\Users\erint\Documents\python programs\bot stuff\Chubonyo_Bot')


class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.is_owner()
    @commands.command(hidden=True)
    async def shutdown(self, ctx):
        print(f"I was shutdown by User: {ctx.author.display_name} ID: {ctx.author.id}")
        embed = discord.Embed(title=f"Shutting down...",
                              color=0xFFCCFF)
        msg = await ctx.send(embed=embed)  # only runs if owner uses command
        await msg.edit(content="", embed=discord.Embed(title=f"shutdown successful",
                                                       color=0xFFCCFF))
        await self.bot.logout()

    @commands.is_owner()
    @commands.command(hidden=True)
    async def reboot(self, ctx):
        extensions = ctx.bot.extensions.copy()
        for name in extensions.keys():
            ctx.bot.reload_extension(name)

        await ctx.send("Bot restarted")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def load(self, ctx, *, extension):
        try:
            self.bot.load_extension(f"exts.{extension}")

        except Exception as ex:
            await self._send_error(ctx, ex)    

        else:
            embed = discord.Embed(
                title=f"Successfully loaded: __{extension}__ :ok_hand_tone1:",
                color=0xFFCCFF,
                timestamp=datetime.datetime.utcnow()
            )
            await ctx.send(embed=embed)

    @commands.is_owner()
    @commands.command(hidden=True)
    async def unload(self, ctx, *, extension):
        try:
            self.bot.unload_extension(f"exts.{extension}")

        except Exception as ex:
            await self._send_error(ctx, ex)    
            
        else:
            embed = discord.Embed(
                title=f"Successfully unloaded: __{extension}__ :ok_hand_tone1:",
                color=0xFFCCFF,
                timestamp=datetime.datetime.utcnow()
            )
            await ctx.send(embed=embed)

    @commands.is_owner()
    @commands.command(hidden=True)
    async def reload(self, ctx, *, extension):
        try:
            self.bot.reload_extension(f"exts.{extension}")

        except Exception as ex:
            await self._send_error(ctx, ex)    
            
        else:
            embed = discord.Embed(
                title=f"Successfully reloaded: __{extension}__ :ok_hand_tone1:",
                color=0xFFCCFF,
                timestamp=datetime.datetime.utcnow()
            )
            await ctx.send(embed=embed)


    """@commands.is_owner()
    @commands.command(hidden=True)
    async def blacklist(self, ctx, user: discord.User):
        if user.id in self.bot.config['blacklisted']:
            bl = self.bot.config['blacklisted']
            bl.remove(user.id)
            self.bot.config['blacklisted'] = bl
            with open('./config.json', 'w') as f:
                json.dump(obj=self.bot.config, fp=f)
                await ctx.send(f"{user.name} is no longer blacklisted.")
        elif user.id not in self.bot.config['blacklisted']:
            bl = self.bot.config['blacklisted']
            bl.append(user.id)
            self.bot.config['blacklisted'] = bl
            with open('./config.json', 'w') as f:
                json.dump(obj=self.bot.config, fp=f)
                await ctx.send(f"{user.name} is now blacklisted.")
        await self.bot.set_config()"""

    @commands.is_owner()
    @commands.command(aliases=['code'], hidden=True)
    async def source(self, ctx, *, command):
        cmd = self.bot.get_command(command)
        if not cmd:
            return await ctx.send('No such command.')
        cmd = inspect.getsource(cmd.callback)

        @libneko.embed_generator(max_chars=1525)
        def make_embed(paginator, page, index):
            return discord.Embed(title=f"{command}'s source",
                                 colour=0xFFCCFF,
                                 description=f"```py\n# Python {sys.version}\n{page}```")

        pag = libneko.EmbedNavigatorFactory(factory=make_embed, max_lines=30)
        for line in cmd.splitlines():
            pag.add_line(line.replace('`', '´'))
        pag.start(ctx=ctx, timeout=60.0)

    async def _send_error(self, ctx, ex):
        ex_msg = "".join(traceback.format_exception(type(ex), ex, ex.__traceback__))
        paginator = commands.Paginator()
        for line in ex_msg.splitlines():
            paginator.add_line(line)

        for page in paginator.pages:
            await ctx.send(page)


def setup(bot):
    bot.add_cog(Admin(bot))
