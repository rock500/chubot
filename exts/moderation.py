from discord.ext import commands
import discord
import datetime


class Moderation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.cooldown(rate=1, per=3, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # ban command
    @commands.bot_has_permissions(ban_members=True)
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, member: discord.User, *, reason: str = None):
        if reason is None:
            reason = "No specified reason"
        if member == ctx.author:
            await ctx.send("You cant kick yourself, idiot >:c")
            return

        try:
            await ctx.guild.ban(member, reason=reason)
            await member.send(f"you were **banned** in **{ctx.guild}** for: `{reason}`")
            await ctx.send(f"*{member}* was **banned** for: `{reason}`")
        except:
            try:
                await ctx.guild.ban(member, reason=reason)
                await ctx.send(f"*{member}* was **banned** for: `{reason}`")
            except:
                await ctx.send(f"{ctx.author.mention} i cant ban that user!")

    @ban.error  # if they don't have perms
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f"{ctx.author.mention} You can't ban people!")
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send(f"{ctx.author.mention} i can't do that!")
        elif isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(f"{ctx.author.mention} you need to tell me who to ban!")
        else:
            raise error

    @commands.cooldown(rate=1, per=3, type=commands.BucketType.user)
    @commands.has_permissions(ban_members=True)
    @commands.guild_only()
    @commands.command()
    async def bans(self, ctx):  # shows a list of bans
        bans = '\n'.join(str(ban.user) for ban in await ctx.guild.bans())
        embed1 = discord.Embed(
            title="Those in a better place",
            description=bans,
            color=0xFFCCFF,
        )
        await ctx.send(embed=embed1)

    @bans.error
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send("You don't have permissions to use this command!")
        else:
            raise error

    @commands.cooldown(rate=1, per=3, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # kick command
    @commands.bot_has_permissions(ban_members=True)
    @commands.has_permissions(ban_members=True)
    async def kick(self, ctx, member: discord.User, *, reason: str = None):
        if reason is None:
            reason = "No specified reason"
        if member == ctx.author:
            await ctx.send("You cant kick yourself, idiot >:c")
            return

        try:
            await ctx.guild.kick(member, reason=reason)
            await member.send(f"you were **kicked** in **{ctx.guild}** for: `{reason}`")
            await ctx.send(f"*{member}* was **kicked** for: `{reason}`"),
        except:
            try:
                await ctx.guild.kick(member, reason=reason)
                await ctx.send(f"*{member}* was **kicked** for: `{reason}`")
            except:
                await ctx.send(f"{ctx.author.mention} i cant kick that user!")

    @kick.error  # if they don't have perms to kick
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f"{ctx.author.mention} You can't kick people!")
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send(f"{ctx.author.mention} i can't kick people!")
        elif isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(f"{ctx.author.mention} you need to tell me who to kick!")
        else:
            raise error

    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    @commands.command()
    async def purge(self, ctx, amount: int = 5, user: discord.Member = None):
        messages = []
        if user is None:
            if amount < 5:
                ctx.sent("I cant purge less then 5 messages >:(")
                return
            elif amount >= 101:
                await ctx.send("I cant purge more the 100 messages!")
                return
            elif amount >= 5:
                amount += 1
                await ctx.channel.purge(limit=amount)
        if user is not None:
            if amount < 5:
                ctx.sent("I cant purge less then 5 messages >:(")
                return
            elif amount >= 101:
                await ctx.send("I cant purge more the 100 messages!")
                return
            elif amount >= 5:
                messages = []
                async for message in ctx.history(limit=amount + 1, oldest_first=False):
                    if message.author.id == user.id:
                        messages.append(message)
                        x = 0
        for message in messages:
            try:
                await message.delete()
                x += 1
            except discord.NotFound:
                pass
        if ctx.author.id and user.id is user.id:
            x -= 1
        if user or amount is None:
            embed2 = discord.Embed(
                title=f"Purged {x} messages",
                description=f"I cleared {x} message's happy now?",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed2.set_footer(text=f'Responsible moderator: {ctx.author.display_name}',
                             icon_url=ctx.author.avatar_url)
        elif user is not None:
            embed2 = discord.Embed(
                title=f"Purged {x} messages",
                description=f"I cleared {x} message's from {user.mention} happy now?",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed2.set_footer(text=f'Responsible moderator: {ctx.author.display_name}',
                             icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed2)

    @purge.error  # if they don't have perms
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f"{ctx.author.mention} You can't purge chat's!")
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send(f"{ctx.author.mention} i can't purge chat's!")
        else:
            raise error


def setup(bot):
    bot.add_cog(Moderation(bot))
