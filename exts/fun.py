import json

import aiohttp
import discord
import datetime
import random

import libneko
import typing
import traceback
import praw
import urllib3

from discord.ext import commands
from PIL import Image, ImageDraw, ImageFont

async def fetch(url: str, method: str = "GET", params=None):
    url = urllib3.util.parse_url(url)
    async with aiohttp.ClientSession() as session:
        if method == "POST":
            async with session.post(url=str(url), data=params) as response:
                return await response.json()
        else:
            async with session.get(str(url)) as response:
                print(response)
                print(await response.json())

                
mcfont = ImageFont.truetype("sga.ttf", size=100)

with open("Config.json") as fp:
    imgpw = json.load(fp)


def make(text, font=mcfont, fg=(0, 0, 0), bg=(255, 255, 255)):
    size = font.getsize(text, stroke_width=5)
    img = Image.new("RGBA", (size[0] + 20, size[1] + 20), bg)
    d = ImageDraw.Draw(img)
    d.text((10, 10), text, fill=fg, font=font)
    return img


class Fun(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    def getAnswer(self, answerNumber):
        if answerNumber == 1:
            return 'Hell yeah!'
        elif answerNumber == 2:
            return 'I guess so bro.'
        elif answerNumber == 3:
            return 'Yes.'
        elif answerNumber == 4:
            return "I wasn't listening try again, smh."
        elif answerNumber == 5:
            return 'Ask again later, annoying >:c'
        elif answerNumber == 6:
            return 'Concentrate and ask again.'
        elif answerNumber == 7:
            return 'i say no.'
        elif answerNumber == 8:
            return 'Outlook not so good.'
        elif answerNumber == 9:
            return 'Smh no.'

    @commands.cooldown(rate=1, per=1, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(name='8ball', aliases=['magic8ball'])
    async def eight_ball(self, ctx, *, input: str = None):
        """The magic 8ball decides your fate"""
        if input is None:
            return await ctx.send(f"{ctx.author.mention} This command needs input, smh.")
        r = random.randint(1, 9)
        fortune = self.getAnswer(r)

        em = discord.Embed(
            description=f"`Question:` {input}\n\n`The Magic 8Ball says:` {fortune}",
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        await ctx.send(embed=em)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['animememe', 'animemes', 'ranimeme', 'ranimememes', 'r/animemes', 'r/animeme'])  # r/Animemes generator
    async def animeme(self, ctx, sort: str = 'hot'):
        """Gets a post from r/animemes

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ('hot', 'new', 'top'):
            return await ctx.send(f'{ctx.author.mention} You can only sort by `hot`, `top` or `new`.')
        async with aiohttp.request('get', f'https://www.reddit.com/r/animemes/{sort}.json?sort=top') as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res['data']['children'])['data']
            title = post['title']
            url = post['url']
            likes = post['ups']
            comments = post['num_comments']

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed.set_footer(text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}")
            embed.set_image(url=post['url'])
            await ctx.send(content=f"`tip`, want to suggest a subreddit? use the `suggest` command!", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['r/blessed', 'r/blessedimages', 'rblessed', 'rblessedimages', 'rblessedimage', 'r/blessedimage', 'blessed'])  # r/blessedimages generator
    async def blessedimages(self, ctx, sort: str = 'hot'):
        """Gets a post from r/Blessed_images

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ('hot', 'new', 'top'):
            return await ctx.send(f'{ctx.author.mention} You can only sort by `hot`, `top` or `new`.')
        async with aiohttp.request('get', f'https://www.reddit.com/r/Blessed_images/{sort}.json?sort=top') as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res['data']['children'])['data']
            title = post['title']
            url = post['url']
            likes = post['ups']
            comments = post['num_comments']

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed.set_footer(text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}")
            embed.set_image(url=post['url'])
            await ctx.send(content=f"`tip`, want to suggest a subreddit? use the `suggest` command!", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['r/blursed', 'r/blursedimages', 'rblursed', 'rblursedimages', 'rblursedimage', 'r/blursedimage','blursed'])  # r/blursedimages generator
    async def blursedimages(self, ctx, sort: str = 'hot'):
        """Gets a post from r/blursedimages

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ('hot', 'new', 'top'):
            return await ctx.send(f'{ctx.author.mention} You can only sort by `hot`, `top` or `new`.')
        async with aiohttp.request('get', f'https://www.reddit.com/r/blursedimages/{sort}.json?sort=top') as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res['data']['children'])['data']
            title = post['title']
            url = post['url']
            likes = post['ups']
            comments = post['num_comments']

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed.set_footer(text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}")
            embed.set_image(url=post['url'])
            await ctx.send(content=f"`tip`, want to suggest a subreddit? use the `suggest` command!", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # rates the user on their coolness
    async def coolrate(self, ctx, user: discord.Member = None):
        """Randomly rates the user on a scale of 1-100%"""
        embed = discord.Embed(
            title="Cool Rate",
            color=0xFFCCFF,
        )
        top = 100
        if user:
            embed.description = f"{user.mention} is {random.randint(0, top)}% cool!"
        else:
            embed.description = f"{ctx.author.mention} is {random.randint(0, top)}% cool!"

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def choose(self, ctx, *, choices: str = None):  # Chooses something randomly from given choices
        """Chooses something randomly from your choice separated by a comma, this thing: ','"""
        choices = choices.split(",")

        if len(choices) <= 1 or choices is None:
            await ctx.send("Smh you need to actually tell me what to choose from.")

        else:
            embed = discord.Embed(
                title="I choose: " + random.choice(choices),
                description="\nYou're welcome",
                color=0xFFCCFF
            )
            await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # dank meme generator
    async def dankmeme(self, ctx, sort: str = 'hot'):
        """Gets a post from r/dankmeme

        Sorts by hot on default but you can sort by hot, top and news"""
        sort = sort.lower()
        if sort not in ('hot', 'new', 'top'):
            return await ctx.send(f'{ctx.author.mention} You can only sort by `hot`, `top` or `new`.')
        async with aiohttp.request('get', f'https://www.reddit.com/r/dankmemes/{sort}.json?sort=top') as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res['data']['children'])['data']
            title = post['title']
            url = post['url']
            likes = post['ups']
            comments = post['num_comments']

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed.set_footer(text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}")
            embed.set_image(url=post['url'])
            await ctx.send(content=f"`tip`, want to suggest a subreddit? use the `suggest` command!", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def echo(self, ctx, *, input: str = None):
        """Echos your input"""
        if input is None:
            await ctx.send(f"{ctx.author.mention} command usage: `c!echo <input>`")
            return
        if input == "@everyone" or input == "@here":
            await ctx.send("No, idiot")
            return
        content = "".join(input)
        await ctx.send(content)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['e', 'et', 'mct'])
    async def enchant(self, ctx, *, text):
        """Translates english to minecraft enchantment table language"""
        alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.! "
        if any(c not in alph for c in text):
            print("Sorry, this text contains invalid characters.")
        img = make(text, fg=(0xdc, 0xdd, 0xdf, 255), bg=(0, 0, 0, 0))
        img.save("tmp.png")
        await ctx.send(file=discord.File("tmp.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['reyebleach', 'r/eyebleach'])  # r/eyebleach generator
    async def eyebleach(self, ctx, sort: str = 'hot'):
        """Gets a post from r/eyebleach

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ('hot', 'new', 'top'):
            return await ctx.send(f'{ctx.author.mention} You can only sort by `hot`, `top` or `new`.')
        async with aiohttp.request('get', f'https://www.reddit.com/r/eyebleach/{sort}.json?sort=top') as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res['data']['children'])['data']
            title = post['title']
            url = post['url']
            likes = post['ups']
            comments = post['num_comments']

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed.set_footer(text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}")
            embed.set_image(url=post['url'])
            await ctx.send(content=f"`tip`, want to suggest a subreddit? use the `suggest` command!", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # rates the user on their gayness
    async def gayrate(self, ctx, user: discord.Member = None):
        """Randomly rates the user on a scale of 1-100%"""
        embed = discord.Embed(
            title="Gay Rate",
            color=0xFFCCFF,
        )
        top = 100
        if user:
            embed.description = f"{user.mention} is {random.randint(0, top)}% gay!"
        else:
            embed.description = f"{ctx.author.mention} is {random.randint(0, top)}% gay!"

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # rates the user on their highness
    async def howhigh(self, ctx, user: discord.Member = None):
        """Randomly rates the user on a scale of 1-100%"""
        embed = discord.Embed(
            title="How High",
            color=0xFFCCFF,
        )

        top = 100
        high = random.randint(0, top)
        dec = random.randint(0, 1000)

        if high != 69:
            if user:
                embed.description = f"{user.mention} is {high}% high!"
            else:
                embed.description = f"{ctx.author.mention} is {high}% high!"

        if high == 69 and dec != 420:
            if user:
                embed.description = f"{user.mention} is {high}% high, SnoopDog would be proud!"
            else:
                embed.description = f"{ctx.author.mention} is {high}% high, SnoopDog would be proud!"

        if high == 69 and dec == 420:
            if user:
                embed.description = f"{user.mention} is {high}.{dec}% high, you know this is a 1 in 100000 chance right? you should show my master ngl"
            else:
                embed.description = f"{ctx.author.mention} is {high}.{dec}% high, you know this is a 1 in 100000 chance right? you should show my master ngl"

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=30, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['song'], brief='Music lyrics', description="Search for the provided song's lyrics")
    async def lyrics(self, ctx, *, query: str = None):
        """Shows you the lyrics to a song!"""
        async with aiohttp.request("get", f"https://some-random-api.ml/lyrics?title={query}") as response:
            response = await response.json()
        if "lyrics" in response:
            @libneko.embed_generator(max_chars=700)
            def make_embed(paginator, page, index):
                em = discord.Embed(title=f"{response['title']} - {response['author']}",
                                   colour=0xFFCCFF,
                                   description=f"```{page}```", timestamp=datetime.datetime.utcnow(),
                                   url=response['links']['genius'])
                em.set_footer(text=f'Requested by {ctx.author.name}', icon_url=ctx.author.avatar_url)
                return em

            pag = libneko.EmbedNavigatorFactory(factory=make_embed, max_lines=30)
            for line in response['lyrics'].splitlines():
                pag.add_line(line)
            await pag.start(ctx=ctx, timeout=60.0)
            return
        await ctx.send(response["error"])

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # meme generator
    async def meme(self, ctx, sort: str = 'hot'):
        """Gets a post from r/memes

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ('hot', 'new', 'top'):
            return await ctx.send(f'{ctx.author.mention} You can only sort by `hot`, `top` or `new`.')
        async with aiohttp.request('get', f'https://www.reddit.com/r/memes/{sort}.json?sort=top') as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res['data']['children'])['data']
            title = post['title']
            url = post['url']
            likes = post['ups']
            comments = post['num_comments']

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF
            )
            embed.set_footer(text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}")
            embed.set_image(url=post['url'])
            await ctx.send(content=f"`tip`, want to suggest a subreddit? use the `suggest` command!", embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["random"])  # random
    async def randomnumber(self, ctx, lower: int = None, upper: int = None):
        """chooses a number from your input"""
        if lower is None:
            await ctx.send(f"{ctx.author.mention} command usage: `randomnumber <input> <input>`")
        elif upper is None:
            await ctx.send(f"{ctx.author.mention} command usage: `randomnumber <input> <input>`")
        else:
            await ctx.send(random.randint(lower, upper))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def roll(self, ctx, dice: int):  # rolls a dice
        """Roles a dice"""
        rolls = dice
        limit = dice
        if dice not in [6, 8, 12, 16, 20]:
            await ctx.send('dice can only be 6, 8, 12, 16 and 20!')
            return
        elif dice in [6, 8, 12, 16, 20]:
            result = (str(random.randint(1, limit)))
            await ctx.send(result)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # Urban dictionary
    async def urban(self, ctx, *, word: str = None):
        """Searches something through the Urban Dictionary"""
        if word is None:
            await ctx.send("What would you like to search? you can't search for nothing smh.")
            return
        try:
            async with aiohttp.request("get", "http://api.urbandictionary.com/v0/define",
                                       params={"term": word}) as resp:
                resp.raise_for_status()
                data = await resp.json()

            e2 = discord.Embed(
                title=f":mag:**Searched:** " + data['list'][0]['word'] + " :mag:",
                description=data['list'][0]['definition'],
                timestamp=datetime.datetime.utcnow(),
                color=0xFFCCFF,
            )
            e2.add_field(
                name="Example(s): ",
                value=data['list'][0]['example'],
                inline=False
            )
            e2.add_field(
                name="UpVotes",
                value=f":thumbsup_tone1: {data['list'][0]['thumbs_up']}",
                inline=True
            )
            e2.add_field(
                name="DownVotes",
                value=f":thumbsdown_tone1: {data['list'][0]['thumbs_down']}",
                inline=True
            )

            return await ctx.send(embed=e2)
        except Exception:
            await ctx.send("Sorry we could find that word!")
            return

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def vs(self, ctx, str1: typing.Union[discord.Member, discord.User, str] = None,
                 str2: typing.Union[discord.Member, discord.User, str] = None):
        """Randomly chooses something from your input"""
        try:
            if str1 is None or str2 is None:
                await ctx.send(f"{ctx.author.mention} command usage: `c!vs <input> <input>`")
                return
            url = 'https://api.imgflip.com/caption_image'
            words = [str1, str2, str2, str1, str1, str2, str2, str1, str2, str2, str1, str1]
            choice = random.choice(words)
            second_choice = str1 if choice != str1 else str2
            params = {
                'username': imgpw["imgflip"]["username"],
                'password': imgpw["imgflip"]["password"],
                'template_id': 245800353,
                'text0': choice,
                'text1': second_choice,
            }
            response = await fetch(url=url, method="POST", params=params)
            image = response["data"]["url"]
            post = response["data"]["page_url"]
            embed = discord.Embed(title=f"{choice} is clearly better than {second_choice}", colour=0xFFCCFF,
                                  timestamp=datetime.datetime.utcnow())
            embed.add_field(
                name="image not showing?",
                value=(f"[Click Here!]({post})"),
                inline=False
            )
            embed.set_image(url=image)
            embed.set_footer(text=f"Requested by {ctx.author.display_name}", icon_url=ctx.author.avatar_url)
            await ctx.send(embed=embed)
        except Exception as ex:
            user = (await self.bot.application_info()).owner
            await user.send(traceback.format_exc())

            await user.send("".join(traceback.format_exception(type(ex), ex, ex.__traceback__)))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # rates the user on their weebness
    async def weebrate(self, ctx, user: discord.Member = None):
        """Randomly rates the user on a scale of 1-100%"""
        embed = discord.Embed(
            title="Weeb Rate",
            color=0xFFCCFF,
        )
        top = 100
        if user:
            embed.description = f"{user.mention} is {random.randint(0, top)}% weeb!"
        else:
            embed.description = f"{ctx.author.mention} is {random.randint(0, top)}% weeb!"

        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Fun(bot))
