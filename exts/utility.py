import asyncio
import aiohttp
import time
import typing
import traceback
import discord
import datetime
import logging
import unicodedata

from decimal import Decimal
from discord.ext import commands
from collections import Counter
from pytz import timezone


class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.format = "%Y/%m/%d %H:%M"
        self.timezone = timezone("UTC")

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['av'])
    async def avatar(self, ctx, *, user: typing.Union[discord.Member, discord.User] = None):
        """Shows a user's enlarged avatar (if possible)."""
        embed = discord.Embed(
            color=0xFFCCFF,
            timestamp=datetime.datetime.utcnow()
        )
        user = user or ctx.author
        avatar = user.avatar_url_as(static_format='png')
        embed.set_author(name=str(user), url=avatar)
        embed.set_image(url=avatar)
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # convert command
    async def convert(self, ctx, amount: float = None, from_: str = None, to_: str = None):
        if amount is None or from_ is None or to_ is None:
            return await ctx.send(f"{ctx.author.mention} command usage: `c!convert <amount> <currency> <currency>`")
        elif amount < 0:
            return await ctx.send("You can't use negative numbers! smh")
        from_ = from_.upper()
        to_ = to_.upper()
        try:
            async with aiohttp.request("get", f"https://api.exchangeratesapi.io/latest?base={from_}") as resp:
                data = await resp.json()
            await ctx.send(f"{amount:.2f} {from_} = {(data['rates'][to_] * amount):.2f} {to_}")
        except:
            await ctx.send(f"{ctx.author.mention} We don't seem to know one or more of these currency's!")

    @convert.error
    async def on_error(self, error):
        user = (await self.bot.application_info()).owner
        tb = error.__traceback__
        user.send(f"```py{tb}```")

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def emoji(self, ctx, emoji: typing.Union[discord.Emoji, discord.PartialEmoji, str] = None):
        """Shows you an enlarged version of an emoji!"""
        if emoji is None:
            return await ctx.send("Try typing an actual emoji? smh.")
        if isinstance(emoji, str):
            codepoints = [ord(c) for c in emoji]
            if codepoints[1:2] == [0xFE0F] and len(codepoints) <= 4 and codepoints != [0x1F3F3, 0xFE0F, 0x200D,
                                                                                       0x1F308]:
                codepoints = [codepoints[0], *codepoints[2:]]
            filename = "-".join(hex(c)[2:] for c in codepoints)
            emoji_url = f"https://github.com/twitter/twemoji/raw/master/assets/72x72/{filename}.png"
            name = emoji
        else:
            emoji_url = emoji.url
            name = emoji.name
        embed = discord.Embed(
            title="emoji name: " + name,
            colour=0xFFCCFF,
            timestamp=datetime.datetime.utcnow()
        )
        embed.set_footer(text=ctx.author.name, icon_url=ctx.author.avatar_url)
        embed.set_image(url=emoji_url)
        await ctx.send(embed=embed)

    @emoji.error
    async def on_error(self, error):
        user = (await self.bot.application_info()).owner
        tb = error.__traceback__
        user.send(f"```py{tb}```")

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['server', 'guild', 'serverinfo', 'here'])
    async def guildinfo(self, ctx: commands.Context):
        """
        returns information about the current guild
        """
        features = ctx.guild.features
        features_message = ""
        for feature in features:
            feature = str(feature).lower().replace("_", " ")
            features_message += f"<:check_mark:713189438125113504> {feature}\n"
        set_features = True
        if features_message == "":
            set_features = False
        has_banner = ("BANNER" in features)
        embed = discord.Embed(title=f'**Information about __{ctx.guild.name}__:**', colour=0xFFCCFF,
                              timestamp=ctx.guild.created_at)
        embed.set_footer(text=f'Created at', icon_url=ctx.guild.owner.avatar_url)
        embed.add_field(name="**Guild Owner**", value=f"{ctx.guild.owner.name}#{ctx.guild.owner.discriminator}")
        embed.add_field(name="**Region**", value=ctx.guild.region)
        embed.add_field(name="**ID**", value=ctx.guild.id)
        if set_features:
            embed.add_field(name="**Features**", value=features_message)
        else:
            embed.add_field(name="**Features**", value="None so far.")
        boosts_message = f"Boost Level: {ctx.guild.premium_tier}\nBoosts: {ctx.guild.premium_subscription_count}"
        embed.add_field(name="**Boosts**", value=boosts_message)
        channels_message = f"<:text_channel:713183384825888860> {str(len(ctx.guild.text_channels))}\n<:voice_channel:713183407747629208> {str(len(ctx.guild.voice_channels))}"
        embed.add_field(name="**Channels**", value=channels_message)
        embed.add_field(name="**Emotes**",
                        value=f"{len(ctx.guild.emojis)}")
        embed.add_field(name="**Humans**",
                        value=str(len(list(filter(lambda member: not member.bot, ctx.guild.members)))))
        embed.add_field(name="**Bots**",
                        value=str(len(list(filter(lambda member: member.bot, ctx.guild.members)))))
        member_by_status = Counter(str(m.status) for m in ctx.guild.members)
        members = f'<:online:713182866703384634> {member_by_status["online"]} ' \
                  f'<:idle:713182899024691280> {member_by_status["idle"]} ' \
                  f'<:dnd:713182927202156614> {member_by_status["dnd"]} ' \
                  f'<:offline:713183001596657724> {member_by_status["offline"]}\n' \
                  f'Total: {ctx.guild.member_count}'
        embed.add_field(name="**Members**", value=members, inline=True)
        embed.add_field(name="**Roles**", value=str(len(ctx.guild.roles) - 1), inline=True)
        if has_banner:
            embed.set_image(url=ctx.guild.banner_url)
        embed.set_thumbnail(url=ctx.guild.icon_url)
        await ctx.send(embed=embed)

    @commands.guild_only()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command(aliases=["maths", "calculator"])
    async def math(self, ctx, *, input):
        """Works just like a calculator"""
        expr = input
        async with aiohttp.request("POST", "http://api.mathjs.org/v4/", json={"expr": expr}) as resp:
            data = await resp.json()
            await ctx.send(
                embed=discord.Embed(color=0xFFCCFF).add_field(
                    name="Input", value=f"`{expr}`"
                ).add_field(
                    name="Output",
                    value=f"`{data['result']}`" if data["error"] is None else f"`{data['error']}`"
                )
            )

    @math.error
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(f"{ctx.author.mention} smh, you need to actually give some input >:c")
            return
        else:
            raise error

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def ping(self, ctx):
        """displays my ping and acknowledgment."""
        start = time.monotonic()
        msg = await ctx.send('Checking Ping...')
        millis = (time.monotonic() - start) * 1000

        heartbeat = ctx.bot.latency * 1000

        embed = discord.Embed(
            title="__Bot Latency__",
            timestamp=(datetime.datetime.utcnow()),
            color=0xFFCCFF
        )
        embed.add_field(
            name="heartbeat:",
            value=f"{heartbeat:,.2f}ms",
            inline=False
        )
        embed.add_field(
            name="Acknowledgement:",
            value=f"{millis:,.2f}ms",
            inline=False
        )
        await msg.edit(content="", embed=embed)

    @commands.cooldown(rate=1, per=30, type=commands.BucketType.user)
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    @commands.command()
    async def setprefix(self, ctx, prefix: str = None):
        if prefix is None:
            await ctx.send("Invalid Command `No prefix specified`")
            return
        async with self.bot.db.acquire() as pconn:
            server = await pconn.fetchval(
                "SELECT guild_id FROM prefixes WHERE guild_id = $1", ctx.guild.id
            )
            if server is None:
                query = """INSERT INTO prefixes (guild_id, prefix)
                            VALUES($1, $2)"""
                args = (ctx.guild.id, prefix)
                await pconn.execute(query, *args)
                await ctx.send("Prefix Updated successfully")
                logger.info(f"Prefix Updated successfully for guild {ctx.guild}")
                return
            await pconn.execute(
                "UPDATE prefixes SET prefix = $1 WHERE guild_id = $2", prefix, ctx.guild.id
            )
            await ctx.send("Prefix Changed Successfully")
            logger.info(f"Prefix Updated successfully for guild {ctx.guild}")


    @setprefix.error  # if they don't have perms
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f"{ctx.author.mention} You don't have permissions to change my prefix!")

    @commands.Cog.listener()
    async def on_connect(self):
        self.bot.started_at = time.perf_counter()

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    async def uptime(self, ctx):
        """Displays how long i've been online"""
        uptime = int(time.perf_counter() - self.bot.started_at)

        mins, secs = divmod(uptime, 60)
        hours, mins = divmod(mins, 60)
        days, hours = divmod(hours, 24)
        secs = round(secs)

        embed = discord.Embed(
            title=("I've been online for"),
            description=f"{days} days, {hours} hours, {mins} mins, {secs} secs",
            color=0xFFCCFF
        )

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=30, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['whois', 'who'])
    async def userinfo(self, ctx, member: discord.Member = None):
        """
        returns information about the provided user or you if no user provided
        """
        if member is None:
            member = ctx.author
        embed = discord.Embed(title=f'**Information about {member.display_name}:**', colour=0xFFCCFF,
                              timestamp=datetime.datetime.utcnow())
        embed.set_footer(text=f'Requested by {ctx.author.display_name}', icon_url=ctx.author.avatar_url)
        embed.set_author(name=' ', icon_url=member.avatar_url, url=member.avatar_url)
        embed.add_field(name=f'**Full Name**', value=f'{member.name}#{member.discriminator}')
        embed.add_field(name=f'**Joined "{ctx.guild.name}" at**',
                        value=self.timezone.localize(member.joined_at).astimezone(self.timezone).strftime(
                            self.format))
        embed.add_field(name='**ID**', value=member.id)

        status = "Unknown"
        status = (
            "<:online:713182866703384634> online"
            if member.status == discord.Status.online
            else status)
        status = (
            "<:dnd:713182927202156614> do not disturb"
            if member.status == discord.Status.dnd or member.status == discord.Status.do_not_disturb
            else status)
        status = (
            "<:idle:713182899024691280> idle"
            if member.status == discord.Status.idle
            else status)
        status = (
            "<:offline:713183001596657724>"
            if member.status == discord.Status.offline
            else status
        )
        platform = "Web"
        platform = (
            "<:web:713204282924466217>"
            if member.web_status not in [discord.Status.offline]
            else platform
        )
        platform = (
            "<:desktop:713204282869940234>"
            if member.desktop_status not in [discord.Status.offline]
            else platform
        )
        platform = (
            "<:mobile:713204282463092747>"
            if member.mobile_status not in [discord.Status.offline]
            else platform
        )
        platform = (
            "<:web:713204282924466217>"
            if member.bot
            else platform
        )
        if member.status == discord.Status.offline:
            status_value = (
                f"{status} offline"
                if status != discord.Status.offline and status != discord.Status.invisible and status != 'Offline'
                else "<:offline:713183001596657724>"
            )
        if member.status != discord.Status.offline:
            status_value = (
                f"{status}  **»**   {platform}"
                if status != discord.Status.offline and status != discord.Status.invisible and status != 'Offline'
                else "<:offline:713183001596657724>"
            )
        embed.add_field(name="**Status**", value=status_value)
        embed.add_field(name="**Created at**",
                        value=self.timezone.localize(member.created_at).astimezone(self.timezone).strftime(
                            self.format))
        boosting_value = (
            f"Boosting since: {self.timezone.localize(member.premium_since).astimezone(self.timezone).strftime(self.format)}"
            if member.premium_since is not None
            else "Not Boosting"
        )
        embed.add_field(name='**Boosting status**', value=boosting_value)
        roles = list(member.roles)
        roles.reverse()
        rolestr = " ".join([role.mention for role in roles if "@everyone" not in role.name])
        if len(member.roles) >= 12:
            rolestr = f"{len(roles) - 1}"
            embed.add_field(name="**Roles**", value=f"{rolestr}")
        else:
            embed.add_field(name=f"**Roles ({len(roles) - 1})**", value=f"{rolestr}")
        embed.set_thumbnail(url=str(member.avatar_url).replace('1024', '2048'))
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['unicode', 'characterinfo'])
    async def charinfo(self, ctx, *, characters: str):
        """Shows you information about a number of characters.
        Only up to 25 characters at a time.
        """
        try:
            def to_string(c):
                digit = f'{ord(c):x}'
                name = unicodedata.name(c, 'Name not found.')
                return f'`\\U{digit:>08}`: {name} | {c} | \N{EM DASH} <http://www.fileformat.info/info/unicode/char/{digit}>'

            msg = '\n'.join(map(to_string, characters))
            if len(msg) > 2000:
                return await ctx.send('Output too long to display.')
            await ctx.send(msg)
        except:
            await ctx.send(f"{ctx.author.mention} command usage: `c!charinfo <input>`")

    @commands.command(hidden=True)
    async def status(self, ctx):
        """Displays discord's status."""
        async with aiohttp.request("get", "https://status.discordapp.com/api/v2/status.json") as resp:
            data = await resp.json()

            url = data['page']['url']
            indic = data['status']['indicator']
            desc = data['status']['description']

            embed = discord.Embed(
                title="Current discord status",
                url=f"{url}",
                color=0xFFCCFF
            )
            embed.add_field(name="Status", value=f"```\n{desc}\n```")
            if desc != "All Systems Operational":
                embed.add_field(name="Severity", value=f"{indic}")

            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Utility(bot))
