import discord
from discord.ext import commands
import datetime


class CustomHelpCommand(commands.HelpCommand):
    async def send_bot_help(self, mapping):
        emb = discord.Embed(
            title="Command list & General info",
            description=f"Use `{self.clean_prefix}help [command]` for more info on a command.",
            timestamp=datetime.datetime.utcnow(),
            colour=0xFFCCFF
        )
        emb.add_field(
            name=":shield: Moderation",
            value="`c!help moderation`",
            inline=True
        )
        emb.add_field(
            name=":toolbox: Utility",
            value="`c!help utility`",
            inline=True
        )
        emb.add_field(
            name=":smile: Fun",
            value="`c!help fun`",
            inline=True
        )
        emb.add_field(
            name=":iphone: Emotes ",
            value="`c!help emotes`",
            inline=True
        )
        emb.add_field(
            name=":books: Misc ",
            value="`c!help misc`",
            inline=True
        )
        emb.add_field(
            name=":wink: Nsfw ",
            value="`c!help nsfw`",
            inline=True
        )
        emb.set_footer(text="use c!help (group) for more help on a group of commands")
        await self.context.send(embed=emb)

    async def send_cog_help(self, cog):
        cog_commands = cog.get_commands()
        e = discord.Embed(
            title=f"{cog.qualified_name} commands",
            description=cog.description or None,
            timestamp=datetime.datetime.utcnow(),
            color=0xFFCCFF
        )
        e.set_footer(text="use c!help (command) for more info on a command")
        e.add_field(
            name="Commands",
            value=", ".join([f"`{c.name}`" for c in cog_commands])
        )
        await self.get_destination().send(embed=e)

    async def send_command_help(self, command):
        emb = discord.Embed(title=f"Detailed help for __{command}__",
                            description=f"```{self.get_command_signature(command)}```",
                            colour=0xFFCCFF
                            )
        if not command.help:
            pass
        else:
            emb.add_field(name="Details", value=f"```{command.help}```" or "No details available.",
                          inline=False)
        if not command.aliases:
            pass
        else:
            emb.add_field(name="Aliases", value=f"```{', '.join(command.aliases)}```",
                          inline=False)
        await self.context.send(embed=emb)

    def get_command_signature(self, command):
        return '{0.clean_prefix}{1.qualified_name} {1.signature}'.format(self, command)


class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._original_help_command = bot.help_command
        bot.help_command = CustomHelpCommand(command_attrs={"aliases": ['h', '?'], "hidden": True})
        bot.help_command.cog = self

    def cog_unload(self):
        self.bot.help_command = self._original_help_command


def setup(bot):
    bot.add_cog(Help(bot))
